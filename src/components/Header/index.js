import styled from "styled-components";

export default styled.h1`
  color: #46a6ed;
  text-align: left;
`;
