import React from "react";
import styled from "styled-components";
import { formatString } from "../../helpers/string";

const DataForm = styled.div`
  padding: 0.3em;
  height: 1.6em;
  background: #eee;
  color: #222;
`;

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
`;

const Header = styled.h3`
  text-align: left;
`;

const Highlight = styled.span`
  color: #46a6ed;
`;

const TableTD = styled.td`
  padding: 0.5em;
  border-bottom: 1px solid #ddd;
  background-color: white;
  cursor: pointer;
`;

const TableTH = styled.th`
  padding: 0.5em;
  background-color: #f5f5f5;
`;

const InfoListComponent = ({ type, userData, setRepresentative }) => {
  return (
    <Wrapper>
      <Header>
        List / <Highlight>{formatString(type)}</Highlight>
      </Header>
      <table>
        <thead>
          <tr>
            <TableTH>Name</TableTH>
            <TableTH>Party</TableTH>
          </tr>
        </thead>
        {userData.map((info, index) => {
          return (
            <tr key={index} onClick={() => setRepresentative({ ...info })}>
              <TableTD>{info.name}</TableTD>
              <TableTD>{info.party}</TableTD>
            </tr>
          );
        })}
      </table>
    </Wrapper>
  );
};

export default InfoListComponent;
