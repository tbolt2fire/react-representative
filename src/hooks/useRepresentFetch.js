import { useEffect, useState, useCallback } from "react";
import { FEATCH_URL } from "../types";

const useRepresentFetch = (type, states) => {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [success, setSuccess] = useState(false);

  const fetchData = useCallback(
    async (type, state) => {
      try {
        setLoading(true);
        const url = `${FEATCH_URL}/${type}/${state}`;
        const response = await fetch(url);
        const jsonResponse = await response.json();
        setSuccess(jsonResponse.success);
        setData(jsonResponse.results);
      } catch {
        setSuccess(false);
      } finally {
        setLoading(false);
      }
    },
    [type, states]
  );

  useEffect(() => {
    console.log(type, states);
    fetchData(type, states);
  }, [type, states]);

  return { isLoading, data, success };
};

export { useRepresentFetch };
