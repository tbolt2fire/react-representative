import styled from "styled-components";
import { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

import Header from "./components/Header";
import InfoListComponent from "./components/InfoListComponent";
import StateComponent from "./components/StateComponent";
import InfoComponent from "./components/InfoComponent";

import { useRepresentFetch } from "./hooks/useRepresentFetch";

const ViewWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const Wrapper = styled.div`
  padding: 50px;
  display: flex;
  flex-direction: column;
`;

function App() {
  const [states, setStates] = useState("");
  const [type, setType] = useState("");

  const [representative, setRepresentative] = useState({});
  const { isLoading, data, success } = useRepresentFetch(type, states);

  return (
    <Wrapper className="App">
      <Header> Who's my Representative? </Header>
      <StateComponent setStates={setStates} setType={setType} />
      <ViewWrapper>
        <InfoListComponent
          type={type}
          userData={!isLoading && success ? data : []}
          setRepresentative={setRepresentative}
        />
        <InfoComponent info={representative} />
      </ViewWrapper>
    </Wrapper>
  );
}

export default App;
