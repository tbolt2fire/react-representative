import React from "react";
import styled from "styled-components";

const DataForm = styled.div`
  margin: 1em;
  padding: 0.3em;
  height: 1.6em;
  background-color: #f5f5f5;
  color: #222;
`;

const Header = styled.h3`
  text-align: left;
`;

const Wrapper = styled.div`
  flex: 1;
`;

const InfoComponent = ({ info = {} }) => {
  const firstName = info.name?.split(" ")[0];
  const lastName = info.name?.split(" ")[1] || "";

  const { district, phone, office } = info;

  return (
    <Wrapper>
      <Header>Info</Header>
      <DataForm placeholder="First Name">{firstName}</DataForm>
      <DataForm placeholder="Last Name">{lastName}</DataForm>
      <DataForm placeholder="District">{district}</DataForm>
      <DataForm placeholder="Phone">{phone}</DataForm>
      <DataForm placeholder="Office">{office}</DataForm>
    </Wrapper>
  );
};

export default InfoComponent;
