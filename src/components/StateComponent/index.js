import styled from "styled-components";
import { STATES, REPRESENTATIVE_TYPES } from "../../types";
import { formatString } from "../../helpers/string";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: left;
  align-items: center;
  & select {
    padding: 5px 10px;
    margin: 10px;
  }
`;

const StateComponent = ({ setStates, setType }) => {
  return (
    <Wrapper>
      Type:
      <select onChange={(evt) => setType(evt.target.value)}>
        {["", ...REPRESENTATIVE_TYPES].map((type) => {
          return <option value={type}>{formatString(type)}</option>;
        })}
      </select>
      States:
      <select onChange={(evt) => setStates(evt.target.value)}>
        {["", ...STATES].map((state) => {
          return <option value={state}>{state}</option>;
        })}
      </select>
    </Wrapper>
  );
};

export default StateComponent;
